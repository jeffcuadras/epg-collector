# coding: utf-8
# Configuration

# Mongo host
MONGO_HOST = "mongodb.konodrac.local"
#MONGO_HOST = "db.konograma.local"
MONGO_PORT = 27017

#String to identify EPG in dataitemschema collection
EPG_TYPE = "EPG"

# Base path
BASE_DATA_PATH = "/mnt/nfs/aflow_inbox"
EPG_PROCESSED_FOLDER = "processed"

#ES
ELASTICSEARCH_URI = ["worker4.konodrac.local:9200", "worker3.konodrac.local:9200"]
ELASTICSEARCH_BULK_SIZE = 1000
ELASTICSEARCH_SCAN_SIZE = 1000
ELASTICSEARCH_DATAITEM_TYPE = "EPG"
ELASTICSEARCH_TIMEOUT = 30
ELASTICSEARCH_BULK_RETRIES = 10

# Konocloud
KONOCLOUD_API_URI = "http://worker1:3000/api"
#KONOCLOUD_API_URI = "http://http.konograma.local:3000/api"
KONOCLOUD_API_USER_GROUP = "cn=Analyst,ou=groups,dc=konodrac,dc=com cn=AudienceFlowSettings,ou=groups,dc=konodrac,dc=com"
KONOCLOUD_API_USER_ID = "admin"

# Slack
SLACK_TOKEN = "xoxp-18390195648-18396570839-82306411701-9e181dd089d2a0ba9fac7c1ad5265515"
SLACK_EPG_USER = 'epg-collector epg'
SLACK_UPDATE_MARKS_USER = 'epg-collector update_marks'
SLACK_IMPORT_MARKS_USER = 'epg-collector import_marks'

# Kafka
KAFKA_URI = 'worker1.konodrac.local:6667'

SLEEP_INTERVAL = 1
